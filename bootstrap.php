<?php

// Define global constants
define('ROOT_PATH', __DIR__);
define('VIEW_PATH', __DIR__ . '/app/Views/templates');

// Bootstrap all classes
require __DIR__.'/vendor/autoload.php';

use App\Core\App;

App::init();

// run request handling
App::$kernel->run();