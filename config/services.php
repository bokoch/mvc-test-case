<?php

/*
 * Here you can register services
 */

use App\Core\App;
use App\Views\View;
use \App\Core\Db;
use \App\Core\Pagination;
use App\Repositories\TaskRepository;
use \App\Repositories\UserRepository;

App::$container->registerService('db_connection', function ($params) {
    return new Db();
});

App::$container->registerService('view', function ($params) {
    return new View();
});

App::$container->registerService('repository', function ($params) {
    switch ($params) {
        case 'task':
            return new TaskRepository();
            break;
        case 'user':
            return new UserRepository();
            break;
        default:
            throw new Exception('Repository implementation not found');
            break;
    }
});