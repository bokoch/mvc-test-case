<?php

/*
 * Database connection parameters (default: MySQL)
 */

return [
    'db_connection' => 'mysql',
    'db_host' => '127.0.0.1',
    'db_port' => '3306',
    'db_name' => 'mvc-test-case',
    'db_username' => 'mysql_dev',
    'db_password' => '88888888',
    'db_charset' => 'UTF8',
];