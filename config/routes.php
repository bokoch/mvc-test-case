<?php
/*
 * Here you can add your routes
 */

use app\Core\App;

App::$router->addRoute('/', 'App\\Controllers\\TaskController::index');

App::$router->addRoute('/add-task', 'App\\Controllers\\TaskController::addTask');
App::$router->addRoute('/store-task', 'App\\Controllers\\TaskController::storeTask');

App::$router->addRoute('/edit-task/{id}', 'App\\Controllers\\TaskController::editTask');
App::$router->addRoute('/update-task/{id}', 'App\\Controllers\\TaskController::updateTask');
App::$router->addRoute('/delete-task/{id}', 'App\\Controllers\\TaskController::deleteTask');

App::$router->addRoute('/login', 'App\\Controllers\\AuthController::showLogin');
App::$router->addRoute('/handle-login', 'App\\Controllers\\AuthController::login');
App::$router->addRoute('/logout', 'App\\Controllers\\AuthController::logout');
