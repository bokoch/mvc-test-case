<?php

namespace App\Models;


use App\Core\App;

abstract class BaseModel
{
    // ID of entity
    protected $id;

    public function getId()
    {
        return $this->id;
    }

    public function setId(int $id)
    {
        $this->id = $id;
    }
}