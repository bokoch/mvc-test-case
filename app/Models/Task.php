<?php

namespace App\Models;


class Task extends BaseModel
{
    private $userName;

    private $email;

    private $taskText;

    private $complete;

    // task completed value
    public const COMPLETED_STATUS = 1;

    // task not completed value
    public const UNCOMPLETED_STATUS = 0;

    /**
     * @return mixed
     */
    public function getUserName()
    {
        return $this->userName;
    }

    /**
     * @param mixed $userName
     */
    public function setUserName($userName): void
    {
        $this->userName = $userName;
    }

    /**
     * @return mixed
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @param mixed $email
     */
    public function setEmail($email): void
    {
        $this->email = $email;
    }

    /**
     * @return mixed
     */
    public function getTaskText()
    {
        return $this->taskText;
    }

    /**
     * @param mixed $taskText
     */
    public function setTaskText($taskText): void
    {
        $this->taskText = $taskText;
    }

    /**
     * @return mixed
     */
    public function getComplete()
    {
        return $this->complete;
    }

    /**
     * @param mixed $complete
     */
    public function setComplete($complete): void
    {
        $this->complete = $complete;
    }

}