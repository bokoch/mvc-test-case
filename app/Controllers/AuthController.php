<?php

namespace App\Controllers;


use App\Core\App;
use App\Core\Http\Request;
use App\Exceptions\HttpNotFoundException;
use App\Models\User;

class AuthController extends BaseController
{
    public function showLogin(Request $request)
    {
        $data = [];
        $layoutData = [];

        $layoutData['title'] = 'Авторизация';

        return $this->getView()->renderView('auth/login', $data, $layoutData);

    }

    /**
     * @param Request $request
     * @throws HttpNotFoundException
     */
    public function login(Request $request)
    {
        $params = $request->getRequest();
        $userRepository = App::$container->getService('repository', 'user');

        // create search array from request parameters
        $searchParams = [
            'login' => $params['login'],
            'password' => $params['password'],
        ];

        $user = $userRepository->findWhereOne($searchParams);

        if (!$user instanceof User) {
            throw new HttpNotFoundException('User with such credentials not found. Try again');
        }

        session_start();

        // put user into session
        $_SESSION['user'] = $user;

        return $this->redirect('/');
    }

    public function logout(Request $request)
    {
        session_start();

        // clear user in session
        unset($_SESSION["user"]);

        return $this->redirect('/');
    }

}