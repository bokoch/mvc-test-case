<?php

namespace App\Controllers;

use App\Core\App;
use App\Core\Db;
use App\Core\Http\Request;
use App\Core\Pagination;
use App\Exceptions\HttpNotFoundException;
use App\Models\BaseModel;
use App\Models\Task;
use App\Models\User;

class TaskController extends BaseController
{
    // limit tasks on one page
    public const PAGE_LIMIT = 3;

    public function index(Request $request)
    {
        $taskRepository = App::$container->getService('repository', 'task');

        $data = [];
        $layoutData = [];
        $queryParams = $request->getQuery();

        $layoutData['title'] = 'Список задач';

        // get pagination
        $currentPage = $queryParams['page'] ?? 1;
        $totalTasks = $taskRepository->count();
        $pagination = new Pagination('/', $totalTasks, $currentPage);

        // get all tasks with request order
        $tasks = $taskRepository->findWhere(
            [],
            ['*'],
            $queryParams['order_by'] ?? 'id',
            $queryParams['order_type'] ?? 'asc',
            self::PAGE_LIMIT,
            self::PAGE_LIMIT * ($currentPage - 1)
        );

        $data['tasks'] = $tasks;
        $data['order_type'] = ($queryParams['order_type'] == 'desc') ? 'asc' : 'desc';
        $data['order_by'] = $queryParams['order_by'] ?? 'id';
        $data['pagination'] = $pagination->getPagination();

        $data['pageIndex'] = $pagination->getCurrentPage();

        return $this->getView()->renderView('task/index', $data, $layoutData);
    }

    public function addTask(Request $request)
    {
        $data = [];
        $layoutData = [];

        $layoutData['title'] = 'Создать задачу';

        return $this->getView()->renderView('task/create', $data, $layoutData);
    }

    public function storeTask(Request $request)
    {
        $params = $request->getRequest();

        //validate request parameters
        if (empty($params['user_name']) || empty($params['email']) || empty($params['task_text'])) {
            return $this->redirect('/add-task');
        }

        $taskRepository = App::$container->getService('repository', 'task');

        $task = new Task();
        $task->setUserName($params['user_name']);
        $task->setEmail($params['email']);
        $task->setTaskText($params['task_text']);
        $task->setComplete(Task::UNCOMPLETED_STATUS);

        $task_id = $taskRepository->create($task);

        return $this->redirect('/');
    }

    /**
     * @param Request $request
     * @param $id
     * @return false|string
     * @throws HttpNotFoundException
     * @throws \App\Exceptions\HttpAccessDeniedException
     */
    public function editTask(Request $request, $id)
    {
        // check permission
        $this->checkAuth();

        $taskRepository = App::$container->getService('repository', 'task');

        $data = [];
        $layoutData = [];
        $layoutData['title'] = 'Редактировать задачу';

        $task = $taskRepository->findById((int) $id);

        if (!$task instanceof Task) {
            throw new HttpNotFoundException('Task with id: ' . $id . ' not found');
        }

        $data['task'] = $task;

        return $this->getView()->renderView('task/edit', $data, $layoutData);
    }

    /**
     * @param Request $request
     * @param $id
     * @throws HttpNotFoundException
     * @throws \App\Exceptions\HttpAccessDeniedException
     */
    public function updateTask(Request $request, $id)
    {
        // check permission
        $this->checkAuth();

        $params = $request->getRequest();

        //validate request parameters
        if (empty($params['user_name']) || empty($params['email']) || empty($params['task_text'])) {
            return $this->redirect('/edit-task/' . $id);
        }

        $taskRepository = App::$container->getService('repository', 'task');

        // check if complete checkbox is checked
        $complete = (!empty($params['complete'])) ? Task::COMPLETED_STATUS : Task::UNCOMPLETED_STATUS;

        $task = $taskRepository->findById((int) $id);

        if (!$task instanceof Task) {
            throw new HttpNotFoundException('Task with id: ' . $id . ' not found');
        }

        $task->setUserName($params['user_name']);
        $task->setEmail($params['email']);
        $task->setTaskText($params['task_text']);
        $task->setComplete($complete);

        $taskRepository->update($task);

        return $this->redirect('/');
    }

    /**
     * @param Request $request
     * @param $id
     * @throws HttpNotFoundException
     * @throws \App\Exceptions\HttpAccessDeniedException
     */
    public function deleteTask(Request $request, $id)
    {
        // check permission
        $this->checkAuth();

        $taskRepository = App::$container->getService('repository', 'task');

        $task = $taskRepository->findById((int) $id);

        if (!$task instanceof Task) {
            throw new HttpNotFoundException('Task with id: ' . $id . ' not found');
        }

        $taskRepository->delete($task);

        return $this->redirect('/');

    }



}