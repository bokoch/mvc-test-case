<?php

namespace App\Controllers;


use App\Core\App;
use App\Exceptions\HttpAccessDeniedException;
use App\Exceptions\HttpNotFoundException;
use App\Views\View;

abstract class BaseController
{
    /**
     * @var \App\Views\View
     */
    private $view;

    public function __construct()
    {
        $this->view = App::$container->getService('view');
    }

    /**
     * Check if user is logged on site
     * @return bool
     * @throws HttpAccessDeniedException
     */
    protected function checkAuth()
    {
        $userRepository = App::$container->getService('repository', 'user');

        session_start();

        if (!isset($_SESSION['user'])) {
            throw new HttpAccessDeniedException('You haven\'t permission for this action! Please login!');
        }
        $user = $_SESSION['user'];

        if (!$userRepository->checkAuth($user->getId())) {
            throw new HttpAccessDeniedException('You haven\'t permission for this action! Please login!');
        }

        return true;
    }

    public function getView()
    {
        return $this->view;
    }

    public function redirect($routePath = '/')
    {
        return header('Location: ' . $routePath);
    }
}