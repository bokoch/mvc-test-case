<?php

namespace App\Controllers;

/**
 * Display exceptions
 * Class ErrorController
 * @package App\Controllers
 */
class ErrorController extends BaseController
{
    /**
     * Errors that provide by bad http request
     * @param \Throwable $e
     * @return false|string
     */
    public function httpRequestError(\Throwable $e)
    {
        return $this->getView()->renderView('errors/http-error', ['exception' => $e]);
    }

    /**
     * All other error handler
     * @param \Throwable $e
     * @return false|string
     */
    public function error500(\Throwable $e)
    {
        return $this->getView()->renderView('errors/error', ['exception' => $e]);
    }
}