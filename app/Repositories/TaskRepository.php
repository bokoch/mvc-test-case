<?php

namespace App\Repositories;


use App\Models\BaseModel;
use App\Models\Task;

class TaskRepository extends BaseRepository
{
    protected $tableName = 'tasks';

    /**
     * @inheritdoc
     */
    protected function modelMapper($fields): BaseModel
    {
        $model = new Task();

        $model->setId($fields['id']);
        $model->setUserName($fields['user_name']);
        $model->setEmail($fields['email']);
        $model->setTaskText($fields['task_text']);
        $model->setComplete($fields['complete']);

        return $model;
    }


    /**
     * @inheritdoc
     */
    protected function prepareModel(BaseModel $model): array
    {
        return [
            'id' => $model->getId(),
            'user_name' => $model->getUserName(),
            'email' => $model->getEmail(),
            'task_text' => $model->getTaskText(),
            'complete' => $model->getComplete(),
        ];
    }
}