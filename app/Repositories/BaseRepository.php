<?php

namespace App\Repositories;

use App\Core\App;
use App\Core\Db;
use App\Exceptions\HttpNotFoundException;
use App\Models\BaseModel;

/**
 * Repository class that handle models
 * Class BaseRepository
 * @package App\Repositories
 */
abstract class BaseRepository
{
    protected $tableName;

    /**
     * @var \App\Core\Db
     */
    private $db;

    public function __construct()
    {
        try {
            $this->db = App::$container->getService('db_connection');
        } catch (\Exception $e) {

        }
    }

    /**
     * Insert model into DB table
     * @param BaseModel $model
     * @return array
     */
    public function create(BaseModel $model)
    {
        $sql = "INSERT INTO " . $this->escapeString($this->tableName) . " (%s) VALUES (%s)";

        $modelArray = $this->prepareModel($model);

        // id field don't need in insert query
        unset($modelArray['id']);

        // get preffix ':' for PDO prepared statement
        $modelFieldNamesPrefixArr = [];
        foreach (array_keys($modelArray) as $item) {
            $modelFieldNamesPrefixArr[] = ':' . $item;
        }

        // string field names of model
        $modelFieldNames = implode(array_keys($modelArray), ', ');
        // string field names of model with prefix ':' for PDO prepared statement
        $modelFieldNamesPrefix = implode($modelFieldNamesPrefixArr, ', ');

        $sql = sprintf($sql, $modelFieldNames, $modelFieldNamesPrefix);

        // execute query and get last inserted id
        return $this->db->executeQuery($sql, $modelArray, Db::INSERT_MODE);
    }

    /**
     * Update model in DB table
     * @param BaseModel $model
     * @return bool
     */
    public function update(BaseModel $model)
    {
        $sql = "UPDATE " . $this->escapeString($this->tableName) . " SET %s WHERE `id`=:id";

        $modelArray = $this->prepareModel($model);

        // format model to PDO prepared statement
        $modelUpdateFields = [];
        foreach (array_keys($modelArray) as $item) {
            // id field don't need in update query
            if ($item != 'id') {
                $modelUpdateFields[] = $this->escapeString($item) . '=:' . $item;
            }
        }

        $sql = sprintf($sql, implode($modelUpdateFields,', '));

        return $this->db->executeQuery($sql, $modelArray, Db::STATUS_MODE);
    }

    /**
     * Delete model from DB table
     * @param BaseModel $model
     * @return false|true
     */
    public function delete(BaseModel $model)
    {
        $sql = "DELETE FROM " . $this->escapeString($this->tableName) . " WHERE id=:id";

        // return false if model not exist
        if (!$this->findById($model->getId()) instanceof BaseModel) {
            return false;
        }

        return $this->db->executeQuery($sql, ['id' => $model->getId()], Db::STATUS_MODE);
    }

    /**
     * @param $id
     * @param array $columns
     * @return BaseModel
     */
    public function findById($id, array $columns = ['*'])
    {
        $columnsEscaped = [];
        foreach ($columns as $column) {
            $columnsEscaped[] = $this->escapeString($column);
        }

        $sql = "SELECT " . implode($columnsEscaped, ', ') . " FROM " . $this->escapeString($this->tableName) . " WHERE id=:id";

        $result = $this->db->executeQuery($sql, ['id' => $id]);

        if (empty($result)) {
            return $result;
        } else {
            return $this->modelMapper($result[0]);
        }
    }

    /**
     * Find all by multiple fields
     * @param array $whereConditions
     * @param array $columns
     * @param string $orderBy
     * @param string $orderType
     * @param int $limit
     * @param int $skip
     * @return mixed
     */
    public function findWhere(
        array $whereConditions = [],
        array $columns = ['*'],
        $orderBy = 'id',
        $orderType = 'ASC',
        $limit = 10,
        $skip = 0
    ) {
        $columnsEscaped = [];
        foreach ($columns as $column) {
            $columnsEscaped[] = $this->escapeString($column);
        }

        $sql = "SELECT " . implode($columnsEscaped, ', ') . " FROM " . $this->escapeString($this->tableName);
        $index = 0;
        foreach ($whereConditions as $key => $value) {
            if ($index == 0) {
                $sql .= " WHERE";
            }

            $sql .= " " . $key . "=:" . $key;

            if ($index < count($whereConditions) - 1) {
                $sql .= " AND";
            }
            $index++;
        }

        switch (strtolower($orderType)) {
            case 'asc':
                $orderType = 'asc';
                break;
            case 'desc':
                $orderType = 'desc';
                break;
            default:
                $orderType = 'asc';
                break;
        }

        $sql .= " ORDER BY " . $this->escapeString($orderBy) . " " . $orderType;

        $sql .= " LIMIT " . (int) $limit;

        $sql .= " OFFSET " . (int) $skip;

        $results = $this->db->executeQuery($sql, $whereConditions);

        return $this->mapResults($results);
    }

    /**
     * Count models by conditions
     * @param array $whereConditions
     * @return int
     */
    public function count(array $whereConditions = [])
    {
        $sql = "SELECT COUNT(id) as `count` FROM " . $this->escapeString($this->tableName);

        $index = 0;
        foreach ($whereConditions as $key => $value) {
            if ($index == 0) {
                $sql .= " WHERE";
            }

            $sql .= " " . $key . "=:" . $key;

            if ($index < count($whereConditions) - 1) {
                $sql .= " AND";
            }
            $index++;
        }

        $result = $this->db->executeQuery($sql, $whereConditions);
        return $result[0]['count'];
    }

    /**
     * Find one by multiple fields
     * @param array $whereConditions
     * @param array $columns
     * @return mixed
     */
    public function findWhereOne(
        array $whereConditions = [],
        array $columns = ['*']
    ) {
        $columnsEscaped = [];
        foreach ($columns as $column) {
            $columnsEscaped[] = $this->escapeString($column);
        }

        $sql = "SELECT " . implode($columnsEscaped, ', ') . " FROM " . $this->escapeString($this->tableName);

        $index = 0;
        foreach ($whereConditions as $key => $value) {
            if ($index == 0) {
                $sql .= " WHERE";
            }

            $sql .= " " . $key . "=:" . $key;

            if ($index < count($whereConditions) - 1) {
                $sql .= " AND";
            }
            $index++;
        }
        $result = $this->db->executeQuery($sql, $whereConditions);

        if (empty($result)) {
            return $result;
        } else {
            return $this->modelMapper($result[0]);
        }
    }

    private function mapResults($results)
    {
        $modelArray = [];
        foreach ($results as $result) {
            $modelArray[] = $this->modelMapper($result);
        }

        return $modelArray;
    }

    private function escapeString($string)
    {
        return "`" . $string . "`";
    }

    /**
     * Convert database result to model
     * @param $fields
     * @return BaseModel
     */
    abstract protected function modelMapper($fields) : BaseModel;

    /**
     * Prepare model before execute SQL query
     * Converting model to associative array
     * @param BaseModel $model
     * @return array
     */
    abstract protected function prepareModel(BaseModel $model): array;
}