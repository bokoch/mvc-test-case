<?php

namespace App\Repositories;


use App\Models\BaseModel;
use App\Models\User;

class UserRepository extends BaseRepository
{
    protected $tableName = 'users';

    /**
     * @inheritdoc
     */
    protected function modelMapper($fields): BaseModel
    {
        $model = new User();

        $model->setId($fields['id']);
        $model->setLogin($fields['login']);
        $model->setPassword($fields['password']);

        return $model;
    }

    /**
     * @inheritdoc
     */
    protected function prepareModel(BaseModel $model): array
    {
        return [
            'id' => $model->getId(),
            'login' => $model->getLogin(),
            'password' => $model->getPassword(),
        ];
    }

    /**
     * Check if user exist
     * @param $id - user id
     * @return mixed
     */
    public function checkAuth($id)
    {
        $searchParams = [
            'id' => $id
        ];
        $user = $this->findWhereOne($searchParams);

        if ($user instanceof User) {
            return true;
        } else {
            return false;
        }
    }
}