<nav aria-label="Page navigation example">
    <ul class="pagination">
        <li class="page-item">
            <a class="page-link" href="<?=$link . '?page=' . $prevPage?>">Предыдущая</a>
        </li>

        <?php for ($i = 1; $i <= $total; $i++) { ?>
            <li class="page-item <?=($i == $currentPage) ? 'active' : ''?>">
                <a class="page-link"
                   href="<?= ($i == 1) ? $link : ($link . '?page=' . $i) ?>"
                ><?=$i?></a>
            </li>
        <?php } ?>

        <li class="page-item"><a class="page-link" href="<?=$link . '?page=' . $nextPage?>">Следующая</a></li>
    </ul>
</nav>