<div class="form-wrapper">

    <h1>Войти</h1>
    <p class="mt-1"><i>По умолчанию login: admin, password: 123</i></p>

    <form action="/handle-login" method="post">
        <div class="input-group mb-3">
            <input type="text"
                   name="login"
                   class="form-control"
                   placeholder="Login *"
                   required
            >
        </div>

        <div class="input-group mb-3">
            <input type="password"
                   name="password"
                   class="form-control"
                   placeholder="Password *"
                   required
            >
        </div>

        <button type="submit" class="btn btn-success">Войти</button>
        <a href="/" class="btn btn-light">Назад</a>

    </form>

</div>