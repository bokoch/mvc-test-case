<div class="mb-4">
    <a href="/add-task" class="btn btn-primary">Добавить задачу</a>
    <?php session_start(); if (!empty($_SESSION['user'])) { ?>
        <a href="/logout" class="btn btn-dark">Выйти</a>
    <?php } else { ?>
        <a href="/login" class="btn btn-dark">Войти</a>
    <?php } ?>

</div>
<?php if (!empty($tasks)) { ?>
    <table class="table table-hover table-bordered">
        <thead class="thead-dark">
            <tr>
                <th scope="col">
                    <a class="clear_a"
                       href="/?order_by=id&order_type=<?=$order_type?><?=($pageIndex > 1) ? '&page=' . $pageIndex : ''?>"
                    >
                        <span>ID</span>
                        <i class="fa fa-fw fa-sort <?=($order_by == 'id') ? ('fa-sort-'. $order_type) : ''?>"></i>
                    </a>
                </th>
                <th scope="col">
                    <a class="clear_a"
                       href="/?order_by=user_name&order_type=<?=$order_type?><?=($pageIndex > 1) ? '&page=' . $pageIndex : ''?>"
                    >
                        <span>Имя пользователя</span>
                        <i class="fa fa-fw fa-sort <?=($order_by == 'user_name') ? ('fa-sort-'. $order_type) : ''?>"></i>
                    </a>
                </th>
                <th scope="col">
                    <a class="clear_a"
                       href="/?order_by=email&order_type=<?=$order_type?><?=($pageIndex > 1) ? '&page=' . $pageIndex : ''?>"
                    >
                        <span>Email</span>
                        <i class="fa fa-fw fa-sort <?=($order_by == 'email') ? ('fa-sort-'. $order_type) : ''?>"></i>
                    </a>
                </th>
                <th scope="col">
                    <a class="clear_a"
                       href="/?order_by=task_text&order_type=<?=$order_type?><?=($pageIndex > 1) ? '&page=' . $pageIndex : ''?>"
                    >
                        <span>Текст задачи</span>
                        <i class="fa fa-fw fa-sort <?=($order_by == 'task_text') ? ('fa-sort-'. $order_type) : ''?>"></i>
                    </a>
                </th>
                <th scope="col">
                    <a class="clear_a"
                       href="/?order_by=complete&order_type=<?=$order_type?><?=($pageIndex > 1) ? '&page=' . $pageIndex : ''?>"
                    >
                        <span>Выполнено</span>
                        <i class="fa fa-fw fa-sort <?=($order_by == 'complete') ? ('fa-sort-'. $order_type) : ''?>"></i>
                    </a>
                </th>
                <?php if (!empty($_SESSION['user'])) { ?>
                    <th scope="col">
                        <a class="clear_a" href="#">
                            <span>Действия</span>
                        </a>
                    </th>
                <?php } ?>
            </tr>
        </thead>
        <?php foreach ($tasks as $task) { ?>
            <tbody>
                <tr>
                    <td><?=$task->getId()?></td>
                    <td><?=$task->getUserName()?></td>
                    <td><?=$task->getEmail()?></td>
                    <td><?=$task->getTaskText()?></td>
                    <td>
                        <input class="big-check" type="checkbox" <?=($task->getComplete() == 1) ? 'checked' : ''?> disabled>
                    </td>
                    <?php if (!empty($_SESSION['user'])) { ?>
                        <td>
                            <a href="/update-task/<?=$task->getId()?>">Редактировать</a> /
                            <a href="/delete-task/<?=$task->getId()?>"
                               onclick="return confirm('Вы уверены что хотите удалить задачу?')"
                            >Удалить</a>
                        </td>
                    <?php } ?>
                </tr>
            </tbody>
        <?php } ?>
    </table>

    <?=$pagination?>
<?php } else { ?>
    <h1>Задач пока еще нету!</h1>
<?php } ?>
