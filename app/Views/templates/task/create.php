<div class="form-wrapper">
    <h1>Добавить задачу</h1>

    <form action="/store-task" method="post">
        <div class="input-group mb-3">
            <input type="text"
                   name="user_name"
                   class="form-control"
                   placeholder="Имя пользователя *"
                   required
            >
        </div>

        <div class="input-group mb-3">
            <input type="email"
                   name="email"
                   class="form-control"
                   placeholder="Email *"
                   required
            >
        </div>

        <div class="input-group mb-3">
            <input type="text"
                   name="task_text"
                   class="form-control"
                   placeholder="Текст задачи *"
                   required
            >
        </div>

        <button type="submit" class="btn btn-success">Добавить</button>
        <a href="/" class="btn btn-light">Назад</a>

    </form>
</div>
