<div class="form-wrapper">
    <h1>Изменить задачу</h1>

    <form action="/update-task/<?=$task->getId()?>" method="post">
        <div class="input-group mb-3">
            <input type="text"
                   name="user_name"
                   class="form-control"
                   placeholder="Имя пользователя *"
                   value="<?=$task->getUserName()?>"
                   required
            >
        </div>

        <div class="input-group mb-3">
            <input type="email"
                   name="email"
                   class="form-control"
                   placeholder="Email *"
                   value="<?=$task->getEmail()?>"
                   required
            >
        </div>

        <div class="input-group mb-3">
            <input type="text"
                   name="task_text"
                   class="form-control"
                   placeholder="Текст задачи *"
                   value="<?=$task->getTaskText()?>"
                   required
            >
        </div>

        <div class="input-group mb-3 ml-4">
            <input class="form-check-input"
                   type="checkbox"
                   value="1"
                   name="complete"
                   id="complete"
                   <?=($task->getComplete() == 1) ? 'checked' : ''?>
            >
            <label class="form-check-label" for="complete">
                Выполнено
            </label>
        </div>

        <button type="submit" class="btn btn-success">Изменить</button>
        <a href="/" class="btn btn-light">Назад</a>

    </form>
</div>
