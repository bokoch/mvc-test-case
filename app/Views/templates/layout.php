<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title><?=$title ?? 'Document'?></title>

    <link href="/public/assets/css/bootstrap.min.css" rel="stylesheet">
    <link href="/public/assets/css/styles.css" rel="stylesheet">
    <link href="/public/libs/font-awesome/css/font-awesome.min.css" rel="stylesheet">

    <script src="/public/assets/js/jquery-3.3.1.min.js" type="text/javascript"></script>
    <script src="/public/assets/js/popper.min.js" type="text/javascript"></script>
    <script src="/public/assets/js/bootstrap.min.js" type="text/javascript"></script>

    <script src="/public/assets/js/main.js" type="text/javascript"></script>

</head>
<body>
<div class="container">
    <div class="row">
        <div class="col-lg-12">
            <div class="main-content mt-5">
                <?=$body?>
            </div>
        </div>
    </div>
</div>
</body>
</html>