<?php

namespace App\Views;

/**
 * Class View
 * @package App\View
 */
class View
{
    private $layoutPath = 'layout.php';

    /**
     * Render HTML response
     * @param $templatePath - path to html template (default path /Views/templates)
     * @param array $data - data that must display in template
     * @param array $layoutData - data that display in layout template
     * @return false|string
     */
    public function renderView($templatePath, array $data = [], array $layoutData = [])
    {
        // get template full path
        $viewTemplate = VIEW_PATH . DIRECTORY_SEPARATOR . $templatePath . '.php';

        // put html into buffer
        ob_start();
        extract($data);
        require_once $viewTemplate;
        $body = ob_get_clean();
        ob_end_clean();

        return $this->renderLayout($body, $layoutData);

    }

    /**
     * Wrapping render html into layout
     * @param $body
     * @param $layoutData
     * @return false|string
     */
    private function renderLayout($body, $layoutData)
    {
        ob_start();
        extract($layoutData);
        require VIEW_PATH . DIRECTORY_SEPARATOR . $this->layoutPath;

        return ob_get_clean();
    }

    /**
     * Render JSON response
     * @param $statusCode
     * @param $data
     * @return false|string
     */
    public function renderJSON($statusCode, array $data = [])
    {
        header('Content-type: application/json');
        http_response_code($statusCode);
        return json_encode($data);
    }

}