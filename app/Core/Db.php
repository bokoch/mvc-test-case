<?php

namespace App\Core;

use PDO;

/**
 * Database handler
 * Class Db
 * @package App\Core
 */
class Db
{
    /**
     * @var \PDO
     */
    private $pdo;

    /**
     * Mode need for inserts and get last insert id
     */
    public const INSERT_MODE = 1;

    /**
     * Mode for update and delete queries. Return true/false status
     */
    public const STATUS_MODE = 2;

    public function __construct()
    {
        $this->getPDO();
    }

    public function executeQuery($query, $params = null, $mode = 0)
    {
        if(is_null($params)){
            $stmt = $this->pdo->query($query);
            return $stmt->fetchAll();
        }

        // show sql errors
        $this->pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        $stmt = $this->pdo->prepare($query);
        $stmt->execute($params);

        switch ($mode) {
            case self::INSERT_MODE:
                return $this->pdo->lastInsertId();
                break;
            case self::STATUS_MODE:
                return true;
                break;
            default:
                return $stmt->fetchAll();
                break;
        }
    }

    /**
     * Create PDO instance
     */
    private function getPDO()
    {
        $config = include ROOT_PATH . DIRECTORY_SEPARATOR . '/config/db.php';

        $this->pdo = new \PDO($this->formatDSN($config), $config['db_username'], $config['db_password'], null);
    }

    /**
     * Format DSN from config
     * @param $config
     * @return string
     */
    private function formatDSN($config)
    {
        return "{$config['db_connection']}:host={$config['db_host']}:{$config['db_port']};dbname={$config['db_name']};charset={$config['db_charset']}";
    }


}