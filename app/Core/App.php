<?php

namespace App\Core;

use App\Controllers\ErrorController;

/**
 * Entry point of application
 * Class App
 * @package App\Core
 */
class App
{
    /**
     * Kernel instance
     * @var \App\Core\Kernel
     */
    public static $kernel;

    /**
     * Router instance
     * @var \App\Core\Router
     */
    public static $router;

    /**
     * DIContainer instance
     * @var \App\Core\DIContainer
     */
    public static $container;

    /**
     * Entry point method
     */
    public static function init()
    {
        self::initServices();
        set_exception_handler([App::class,'handleException']);
    }

    /**
     * Initialize basic services
     */
    public static function initServices()
    {
        self::$kernel = Kernel::getInstance();
        self::$router = Router::getInstance();
        self::$container = DIContainer::getInstance();
    }

    public function handleException (\Throwable $e)
    {
        if($e instanceof \App\Exceptions\HttpException) {
            echo static::$kernel->fireAction(ErrorController::class, 'httpRequestError', [$e]);
        }else{
            echo static::$kernel->fireAction(ErrorController::class, 'error500', [$e]);
        }
    }

}