<?php

namespace App\Core;

use App\Core\Http\Request;
use App\Exceptions\HttpNotFoundException;

/**
 * Resolve all incoming requests
 * Class Router
 * @package App\Core
 */
class Router
{
    // Instance of Router class
    private static $_instance;

    // Array of available routes
    private $routes;

    /**
     * Singleton pattern
     */
    private function __construct() {}
    private function __clone() {}

    public static function getInstance()
    {
        if (empty(self::$_instance)) {
            self::$_instance = new Router();
        }
        return self::$_instance;
    }

    /**
     * Load all routes from config file
     */
    public function loadRoutes()
    {
        require_once ROOT_PATH . '/config/routes.php';
    }

    /**
     * Add route into routes array
     * @param $route
     * @param $handler - request handler in format <controller class>::<controller method>
     * example: 'CommonController::index'
     */
    public function addRoute($route, $handler)
    {
        $this->routes[$route] = $handler;
    }

    /**
     * Get routes array
     * @return mixed
     */
    public function getRoutes()
    {
        return $this->routes;
    }

    /**
     * Parse route and check if request route available in router list
     * @return mixed
     */
    public function resolve()
    {
        $uriParsed = parse_url($_SERVER['REQUEST_URI']);

        // get route path from request
        $path = $uriParsed['path'];

        $request = $this->getRequest($uriParsed['query'] ?? '');

        foreach ($this->routes as $key => $value)
        {
            if ($this->isRouteMatch($key, $path)) {
                $pattern = $key;
                $handler = $value;
            }
        }

        if (empty($pattern) || empty($handler)) {
            throw new HttpNotFoundException('Route not found');
        }

        return $this->callHandler($path, $pattern, $handler, $request);
    }

    private function getRequest($query)
    {
        $queryParams = [];
        // get GET parameters from request
        if (!empty($query)) {
            parse_str($query, $queryParams);
        }

        // get POST parameters from request
        $requestParams = $_POST;

        return new Request($queryParams, $requestParams);
    }

    /**
     * Check if current route match available route
     * @param $pattern
     * @param $route
     * @return false|int
     */
    private function isRouteMatch($pattern, $route)
    {
        $regexPattern = '(\{\w+\})';

        // replace placeholders (string with format like: {example} )
        // into regex pattern (\w+) - means any words
        $formattedPattern =  preg_replace($regexPattern, '(\w+)', $pattern);

        // match current route with pattern
        return preg_match("~^$formattedPattern$~", $route);
    }

    /**
     * Create controller and run handler method
     * @param $route - Request route
     * @param $pattern - Pattern that match request route
     * @param $handler - Controller and method handler
     * @param Request $request
     * @return mixed
     */
    private function callHandler($route, $pattern, $handler, Request $request)
    {
        list($controllerName, $actionName) = explode('::', $handler);

        // split route and pattern
        $patternList = explode('/', $pattern);
        $routeList = explode('/', $route);

        // request parameters from route
        $params['request'] = $request;
        foreach ($patternList as $key => $value) {
            // get all placeholders (ex.: {example})
            if (preg_match('(\{\w+\})', $value)) {
                // remove brackets
                $trimVal = preg_replace('(\{|\})', '', $value);

                // put request parameter to array
                $params[$trimVal] = $routeList[$key];
            }
        }

        return App::$kernel->fireAction($controllerName, $actionName, $params);
    }

}