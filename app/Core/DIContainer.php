<?php

namespace App\Core;

/**
 * Dependency injection container
 * Class DIContainer
 * @package App\Core
 */
class DIContainer
{
    // services array
    protected $services = [];

    private static $_instance;

    private function __construct() {}

    /**
     * Add service to array
     * @param $serviceName
     * @param callable $callable
     */
    public function registerService($serviceName, callable $callable)
    {
        $this->services[$serviceName] = $callable;
    }

    /**
     * Retrieve service
     * @param $serviceName
     * @param $params
     * @return mixed
     * @throws \Exception
     */
    public function getService($serviceName, $params = '')
    {
        if ( !array_key_exists($serviceName, $this->services) ) {
            throw new \Exception("Service {$serviceName} not found!");
        }
        return $this->services[$serviceName]($params);
    }

    public static function getInstance()
    {
        return self::$_instance ?? new DIContainer();
    }

    /**
     * Get all services
     * @return array
     */
    public function getServices()
    {
        return $this->services;
    }

    /**
     * Load service from config file
     */
    public function loadServices()
    {
        require_once ROOT_PATH . '/config/services.php';
    }
}