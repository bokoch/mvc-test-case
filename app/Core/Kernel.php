<?php

namespace App\Core;

/**
 * Core of application
 * Need for initialize services
 * Class Kernel
 * @package App\Core
 */
class Kernel
{
    // Instance of Kernel class
    private static $_instance;

    /**
     * Singleton pattern
     */
    private function __construct() {}
    private function __clone() {}

    public static function getInstance()
    {
        if (empty(self::$_instance)) {
            self::$_instance = new Kernel();
        }
        return self::$_instance;
    }

    /**
     * Run application
     */
    public function run()
    {
        App::$router->loadRoutes();
        App::$container->loadServices();

        // run route resolver and display response
        echo App::$router->resolve();

    }

    /**
     * Create controller and execute handler method
     * @param $controllerName
     * @param $actionName
     * @param array $params
     * @return mixed
     */
    public function fireAction($controllerName, $actionName, $params = [])
    {
        $controllerInstance = new $controllerName;

        // call controller method handler
        return call_user_func_array([$controllerInstance, $actionName], $params);
    }

}