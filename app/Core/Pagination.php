<?php

namespace App\Core;

/**
 * Create pagination
 * Class Pagination
 * @package App\Core
 */
class Pagination
{
    // path to pagination HTML template
    private $templatePath = 'helpers/pagination';

    // current page number
    private $currentPage;

    // route where need pagination
    private $link;

    // total count of items
    private $total;

    // limit of items on one page
    private $pageLimit;

    public function __construct($link, $total, $currentPage = 1 , $pageLimit = 3)
    {
        $this->link = $link;
        $this->currentPage = $currentPage;
        $this->total = $total;
        $this->pageLimit = $pageLimit;
    }

    public function getPagination()
    {
        $data = [];
        $data['link'] = $this->link;
        $data['currentPage'] = $this->currentPage;
        $total = ceil($this->total / $this->pageLimit);
        $data['total'] = $total;

        // get previous page index
        $data['prevPage'] = ($this->currentPage - 1 <= 1) ? 1 : $this->currentPage - 1;
        // get next page index
        $data['nextPage'] = ($this->currentPage + 1 > $total) ? $total : $this->currentPage + 1;

        return ($this->total <= $this->pageLimit) ? '' : $this->renderHTML($data);
    }

    public function renderHTML($data = [])
    {
        ob_start();
        extract($data);
        require VIEW_PATH . DIRECTORY_SEPARATOR . $this->templatePath . '.php';

        return ob_get_clean();
    }

    /**
     * @return int
     */
    public function getCurrentPage(): int
    {
        return $this->currentPage;
    }

    /**
     * @param int $currentPage
     */
    public function setCurrentPage(int $currentPage): void
    {
        $this->currentPage = $currentPage;
    }

    /**
     * @return mixed
     */
    public function getLink()
    {
        return $this->link;
    }

    /**
     * @param mixed $link
     */
    public function setLink($link): void
    {
        $this->link = $link;
    }

    /**
     * @return mixed
     */
    public function getTotal()
    {
        return $this->total;
    }

    /**
     * @param mixed $total
     */
    public function setTotal($total): void
    {
        $this->total = $total;
    }

    /**
     * @return int
     */
    public function getPageLimit(): int
    {
        return $this->pageLimit;
    }

    /**
     * @param int $pageLimit
     */
    public function setPageLimit(int $pageLimit): void
    {
        $this->pageLimit = $pageLimit;
    }


}