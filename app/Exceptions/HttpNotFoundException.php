<?php

namespace App\Exceptions;

use Throwable;

/**
 * Error 404 (Not found)
 * Class HttpNotFoundException
 * @package App\Exceptions
 */
class HttpNotFoundException extends HttpException
{
    public function __construct(string $message = "Not found")
    {
        parent::__construct($message, 404);
    }
}