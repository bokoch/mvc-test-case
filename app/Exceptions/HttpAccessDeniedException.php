<?php

namespace App\Exceptions;

/**
 * Error 403 (Access denied)
 * Class HttpAccessDeniedException
 * @package App\Exceptions
 */
class HttpAccessDeniedException extends HttpException
{
    public function __construct(string $message = "Not found")
    {
        parent::__construct($message, 403);
    }
}