<?php

namespace App\Exceptions;

/**
 * Base HTTP exception
 * Class HttpException
 * @package App\Exceptions
 */
class HttpException extends \Exception
{

}